from player import *

class Game:
    def __init__(self,teams : List[Team],deck:Deck):
        assert len(teams) == 2
        self.teams = teams
        self.deck = deck
        self.scores = [0,0]
        self.atout : Optional[Color]
        self.players : List[Player] = []
        self.first : Optional[int] = random.randrange(4)
        for i in range(2):
            for team in teams:
                self.players.append(team[i])

    def distribute(self):
        """Give 3+2 card to each player"""
        players : List[Player] = [self.players[(self.first+i)%4] for i in range(4)]
        for player in players:
            player.draw(3)
        for player in players:
            player.draw(2)
        
        self.deck.reveal()
    
    def card_picking(self) -> Union[Tuple[bool,Card,Color,Player],Tuple[bool]]:
        """each player can accept or refuse the card if this card is accepted by a player, it's color becomes the atout and func return True else the func return False"""
        players : List[Player] = [self.players[(self.first+i)%4] for i in range(4)]
        top : Card = self.deck.top
        picker : Optional[Player]
        picker = None
        print(f"{players[0].name} begins")
        #first turn if a player pick the card, it's color become the atout color
        for player in players:
            if player.accept(top):
                self.atout = top.color
                player.draw()
                picker = player
                break
        if picker:
            for player in players:
                if player == picker:
                    player.draw(2)
                else:
                    player.draw(3)
            return True,top,top.color,picker
        
        #second turn if a player pick the card ha can choose the atout color
        for player in players:
            color = player.select_color(top)
            if color:
                self.atout = color
                player.draw()
                picker = player
                break
        if picker:
            for player in players:
                if player == picker:
                    player.draw(2)
                else:
                    player.draw(3)
            return True,top,color,picker
        return (False,)

    def __repr__(self):
        return f"current game state :\n{self.deck}\n {self.teams[0]}\n {self.teams[1]}\n"

if __name__ == "__main__":
    deck = Deck(shuffled_card_list())
    picked = False

    while not picked:
        players = [Human(Hand(deck),'North')]+[IA(Hand(deck),name) for name in ["South","East",'West']]
        teams = [Team(players[:2]),Team(players[2:])]
        
        root = Game(teams,deck)
        print(root)
        root.distribute()
        print(root)
        result = root.card_picking()
        
        if result[0]:
            picked,top,color,player = result
            print(f"{player.name} picked {top} the atout color is {color}")
        else:
            print("No one picked a card")
            for player in players:
                player.hand.empty()
    
    print(root)