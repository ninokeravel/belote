from cards import *
from cards import Card, Hand, Optional

class Player:
    def __init__(self,hand:Hand,name:str) -> None:
        self.hand = hand
        self.name = name
    
    def __repr__(self) -> str:
        return f"{self.name} : {self.hand}"
    
    def draw(self,amount:int=1):
        self.hand.draw(amount)

    def accept(self,card:Card) -> bool:
        return False
    
    def select_color(self,card:Card) -> Optional[int]:
        return None

class IA(Player):
    def __init__(self, hand: Hand, name: str) -> None:
        super().__init__(hand, name)

    def accept(self,card:Card) -> bool:
        return random.choice([True,False])
    
    def select_color(self,card:Card) -> Optional[int]:
        return random.choice([None]+[c for c in Color if c!=card.color])  
    


class Human(Player):
    def __init__(self,hand:Hand,name:Optional[str]=None):
        if not name:
            name = input("Name of the human player : ")
        super().__init__(hand,name)
    
    def accept(self, card: Card) -> bool:
        if input(f"Do you want to take {card} and that {card.color} becomes the atout [Y/n]? : ") in 'Yy':
            return True
        return False

    def select_color(self, card: Card) -> Optional[int]:
        if input(f"Do you want to pick {card} and choose the atout color ? [Y/n] : ") in 'Yy':
            color_list = [color for color in Color if color!=card.color]
            print("Avalable Color :\n")
            for i in range(3):
                print(f"\t{i}. {color_list[i]}")
            return color_list[int(input("What color do you choose ? [0/1/2]"))]
        return None

class Team:
    def __init__(self,players : List[Player]):
        assert len(players) == 2
        self.members = players
    
    def __repr__(self) -> str:
        return f"{self.members[0]} - {self.members[1]}"
    
    def __getitem__(self,key:int) -> Player:
        return self.members[key]




