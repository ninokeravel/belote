from enum import Enum
from typing import List,Optional,Tuple,Union,Type
import random

#creates 4 colors
class Color(Enum):
	HEART,SPADE,DIAMOND,CLOVER = range(4)

#creates 8 ranks
class Rank(Enum):
	AS,SEVEN,HEIGHT,NINE,TEN,JACK,QUEEN,KING = range(8)

class Card:
	def __init__(self,color:Color,rank:Rank):
		self.color = color
		self.rank = rank
	
	def __eq__(self, other: 'Card') -> bool:
		return self.color == other.color and self.rank==other.rank
	
	def __repr__(self):
		return f"({str(self.rank)} {str(self.color)})"


class Deck:
	def __init__(self,card_list: Optional[List[Card]] = None):
		self.cards : List[Card] = []
		if card_list == None:
			for color in Color:
				for rank in Rank:
					self.cards.append(Card(color,rank))
		else:
			for card in card_list:
				self.cards.append(card)
		self.top : Optional[card] = None
	
	def draw(self,amount:int)->List[Card]:
		togive : List[Card] = []
		for i in range(amount):
			togive.append(self.cards.pop())
		return togive
	
	def reveal(self):
		self.top = self.cards[-1]

	def __repr__(self):
		if self.top:
			return f"Deck of {len(self.cards)} cards with {self.top} on top"
		return f"Deck of {len(self.cards)} cards"


class Hand:
	def __init__(self,deck:Deck):
		self.cards : List[Card] = []
		self.deck = deck
	
	def draw(self,amount:int):
		drawn = self.deck.draw(amount)
		for card in drawn:
			self.cards.append(card)
	
	def empty(self):
		for i in range(len(self.cards)):
			self.deck.cards.append(self.cards.pop())

	def __repr__(self):
		return str(self.cards)

def shuffled_card_list() -> List[Card]:
	cards : List[Card]= []
	for color in Color:
		for rank in Rank:
			cards.append(Card(color,rank))
	random.shuffle(cards)
	return cards

if __name__ == "__main__":
	deck = Deck(shuffled_card_list())
	hand1 = Hand(deck)
	hand1.draw(3)
	print(deck)
	print(hand1)